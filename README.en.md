English | [简体中文](./README.md)

**This page was translated by Google**

<h1 align="center">
<b>DigitalCube Development Kit</b>
</h1>
<div align="center">Digitcube is a simple and easy-to-use visual large-screen front-end framework, developed based on vue2</div>

<p align="center">
   <a href="http://124.222.103.91:8080/">Documentation</a> •
   <a href="http://124.222.103.91/">Demo Site</a> •
   <a href="https://www.npmjs.com/package/digitcube-core">Core library</a> •
   <a href="https://www.youtube.com/@ayin86cn">Youtube</a>
</p>


## 🛡️ Declaration

**<span style="color:red">The Temporary license built into this development kit is only used for testing, experience and learning. Unable to debug, unable to package and deploy. </span>**

The files in this development kit are development source codes, which can be used for commercial project development only by updating the license. For details, please contact the administrator.



- Free users can learn data visualization development ideas, design, themes, multi-language, style, layout, and the use of echarts through this development kit
- Authorized users, contact the administrator to update the license and go directly to the development stage.



**This development kit has 3 built-in data screens, as shown in the figure below. **

**Data Screen A**-Support map drill-down data linkage

<img src="./demo/screenA.png" style="border-radius:10px" width="800" />

 **Data Screen C**

<img src="./demo/screenC.png" style="border-radius:10px" width="800" />

 **Data Screen D**

<img src="./demo/screenD.png" style="border-radius:10px" width="800" />

**This development kit has 3 built-in functional interfaces, namely 3D panel, tech border and chart colour matching tool**

 **3D panel**

<img src="./demo/board3D.png" style="border-radius:10px" width="800" />

 **Tech border**

<img src="./demo/techBorder.png" style="border-radius:10px" width="800" />

 **Chart Colour Matching Tool**

<img src="./demo/chartPalette.png" style="border-radius:10px" width="800" />



**The source code for the development of the mobile data screen is located in the group file of QQ Group:119059920, please pick it up yourself.**

 **Data Screen A-Mobile** 

<img src="./demo/screenA-mobile.png" style="border-radius:10px" width="300" />



**For the following data screens and more demos, please check out [Digital Cube Demo Site](http://124.222.103.91/)** 

 **Data Screen B**

<img src="./demo/screenB.png" style="border-radius:10px" width="800" />

 enterprise splash screen 4x2

<img src="./demo/splicingScreens.png" style="border-radius:10px" width="800" />


## ✨Features

- **🖥️ Full Ended Adaptation**

  The perfect all-end adaptation solution for PCs, mobile phones, tablets, corporate splicing screens, so to speak, any terminal device can be perfectly adapted, the industry's leading adaptation solution.

- **🎨 Powerful built-in themes**

  One click to switch styles, all elements, elements and details of everything are perfectly supported theme switching. Themes can be developed quickly and customised according to customer requirements using the built-in theme designer.

- **🛸Free development without limitations**

  Using webpack, vue2 and other popular technology stack, by calling self-developed components and chart components, you can quickly deploy online without too much tedious development process, greatly shortening the development cycle. Truly free development at source level.

- **🧩De-bitmap to fully use SVG**

  Full vectorisation (de-bitmap), due to the special use scenario of visualisation of large screens, the use of traditional bitmap situation graphics zoom details blurred, while the use of vector graphics details scaled can still maintain the original details clarity.

- **📊Enterprise splicing screen**

  In the face of the enterprise splicing screen, we have a very large number of display solutions, according to the customer's splicing screen equipment situation for custom development. The best display effect can be achieved under any equipment.


- **🌈 Intelligent colour matching for charts**

  With this framework, you can say goodbye to the headache of colour matching for charts. It provides the ability to colour match charts intelligently and the chart colouring tool.

- **🗺️GEO 3D Maps**

  Based on the GEO Json format, the map is easy to use and can meet most of the scenarios, you can place any data on the map such as points, lines, surfaces and graphics. The map can be presented in a flat or 3D format.

- **🧑‍ Internationalisation**

  Aimed at global business and expanding market coverage, internationalisation can improve user satisfaction, reduce development and maintenance costs and increase the scalability of the software.

- **🚀Steady iterations for rapid response**

  The framework has gone through two major releases and numerous minor iterations, with timely bug fixes and steady feature expansion. For paid users can be achieved in a timely response.





## 📖 Installation Tutorial

1. `npm i` installation dependencies
2. `npm run serve` to start the project to preview

Please refer to [documentation](http://124.222.103.91:8080/) for development package description and usage



## 🌟Licensing

If you need to purchase a license or negotiate with us, please contact us on WeChat for details.

Please indicate **company+job** in the verification message when you add the WeChat, applications without verification information will be ignored.

If you are verified, please send a photo of your position, such as a job title or work badge, if possible. Also indicate your purpose.

Refer to the options below.

- Request for quotation
- Custom development
- Purchase of a licence
  - Licences with term
  - Indefinite licence
  - Bulk purchase of licenses 
- Negotiation of cooperation

For a description of the licence, please refer to [Licence Description](http://124.222.103.91:8080/license/license#%E4%BB%8B%E7%BB%8D)

**Please open WeChat and scan the code to add customer service, this WeChat signal, only for business negotiations. Please open WeChat and scan the code to add customer service**

<img src="./demo/QRcode.png" width="300" />

For overseas customers, please contact ayin86cn@gmail.com via email

Data Visualisation TechGroupQQ Group:119059920 Technical Communication
